ics-ans-role-rsync-client
=========================

Ansible role to configure rsync on an In-Kind EEE server to synchronize the
epics and startup folders with the ESS EEE server.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.10.1

Role Variables
--------------

```yaml
rsync_client_source: owncloud01.esss.lu.se:80
rsync_client_epics_src: "{{rsync_client_source }}/epics"
rsync_client_epics_timer: 10min
rsync_client_startup_src: "{{rsync_client_source }}/startup"
rsync_client_startup_timer: 15s
rsync_client_dest: /export
# User used for EPICS synchronization
rsync_client_ess_gid: 555
rsync_client_ess_uid: 555
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rsync-client
```

License
-------

BSD 2-clause
