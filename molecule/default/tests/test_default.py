import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_rsync_services_enabled(host):
    # Don't check that the service is running because
    # it doesn't run forever
    assert host.service("rsync-epics.service").is_enabled
    assert host.service("rsync-startup.service").is_enabled


def test_rsync_timers_running_and_enabled(host):
    for item in ('rsync-epics.timer', 'rsync-startup.timer'):
        timer = host.service(item)
        assert timer.is_running
        assert timer.is_enabled


def test_eee_local_directories(host):
    for item in ('/export/epics/modules', '/export/startup/boot'):
        directory = host.file(item)
        assert directory.is_directory
        assert directory.user == 'ess'
