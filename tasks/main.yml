---
- name: install rsync
  yum:
    name: rsync
    state: present

- name: create the ess group
  group:
    name: ess
    gid: "{{ rsync_client_ess_gid }}"

- name: create the ess user
  user:
    name: ess
    uid: "{{ rsync_client_ess_uid }}"
    group: ess
    system: true
    createhome: false
    shell: /sbin/nologin

- name: create the folders to sync
  file:
    path: "{{ item }}"
    state: directory
    owner: ess
  with_items:
    - "{{ rsync_client_dest }}/epics"
    - "{{ rsync_client_dest }}/startup"

# We don't use with_items due to the handler
# If an item changes, all the handlers would be triggered
- name: create the rsync-epics service
  template:
    src: rsync-epics.service.j2
    dest: /etc/systemd/system/rsync-epics.service
    owner: root
    group: root
    mode: 0644
  notify:
    - restart rsync-epics.service

- name: create the rsync-epics timer
  template:
    src: rsync-epics.timer.j2
    dest: /etc/systemd/system/rsync-epics.timer
    owner: root
    group: root
    mode: 0644
  notify:
    - restart rsync-epics.timer

- name: create the rsync-startup service
  template:
    src: rsync-startup.service.j2
    dest: /etc/systemd/system/rsync-startup.service
    owner: root
    group: root
    mode: 0644
  notify:
    - restart rsync-startup.service

- name: create the rsync-startup timer
  template:
    src: rsync-startup.timer.j2
    dest: /etc/systemd/system/rsync-startup.timer
    owner: root
    group: root
    mode: 0644
  notify:
    - restart rsync-startup.timer

# Don't check that rsync services are started
# (that would not be idempotent) because they don't run forever
# The services are started by the handlers (and timers)
- name: ensure rsync services are enabled
  systemd:
    name: "{{item}}"
    daemon_reload: true
    enabled: true
  with_items:
    - rsync-epics.service
    - rsync-startup.service

- name: ensure rsync timers are enabled and started
  systemd:
    name: "{{item}}"
    daemon_reload: true
    enabled: true
    state: started
  with_items:
    - rsync-epics.timer
    - rsync-startup.timer
